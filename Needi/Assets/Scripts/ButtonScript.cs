﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonScript : MonoBehaviour
{
    // --------------------------------------------
    public string itemBeingSold;
    public bool unlocked = false;

    public GameObject itemImage;
    public GameObject coinImage;
    public GameObject cost;
    public GameObject descriptionGO;
    public GameObject lockedImage;

    // --------------------------------------------
    //Checks  if the button can unlock
    public void UnlockCheck()
    {
        if (PetManager.Instance.petScript.PetLevel >= UnlockManager.Instance.GetUnlockLevel(itemBeingSold))
        {
            unlocked = true;
            SetButtonActive();
        }
    }

    // --------------------------------------------

    public void SetButtonActive()
    {
        Color colour = itemImage.GetComponent<Image>().color;
        colour.a = 255;
        itemImage.GetComponent<Image>().color = colour;

        colour = coinImage.GetComponent<Image>().color;
        colour.a = 255;
        coinImage.GetComponent<Image>().color = colour;

        colour = cost.GetComponent<Text>().color;
        colour.a = 255;
        cost.GetComponent<Text>().color = colour;

        colour = descriptionGO.GetComponent<Text>().color;
        colour.a = 255;
        descriptionGO.GetComponent<Text>().color = colour;

        lockedImage.SetActive(false);
    }
}
