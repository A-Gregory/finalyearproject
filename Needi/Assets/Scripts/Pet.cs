﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pet : MonoBehaviour
{
    //Events
    public delegate void PetGameEventArgs();
    public static event PetGameEventArgs OnPetLevelUp;
    public static event PetGameEventArgs OnFunNeedChange;

    private float _hunger = 70;
    public float Hunger { set { _hunger = value; } get { return _hunger; } }

    private float _hygiene = 70;
    public float Hygiene { set { _hygiene = value; } get { return _hygiene; } }

    private float _fun = 70;
    public float Fun { set { _fun = value; } get { return _fun; } }

    private float _energy = 70;
    public float Energy { set { _energy = value; } get { return _energy; } }

    private int _petLevel = 0;
    public int PetLevel { get { return _petLevel; } }

    private DateTime _petStartTime;
    public DateTime PetStartTime
    {
        set { _petStartTime = value; }
        get { return _petStartTime; }
    }

    private Dictionary<string, float> needsDictionary = new Dictionary<string, float>();
    private int minsInDay = 1;
    private int hoursInDay = 24;
    private float restingPoint = 40;
    private float decreaseTime;
    private float decreaseAmount;
    private int lowNeeds = 0;
    private bool canAct = true;

    //------------------------------------------------

    public void Init()
    {
        UIManager.Instance.EditBar("Hunger", Hunger);
        UIManager.Instance.EditBar("Hygiene", Hygiene);
        UIManager.Instance.EditBar("Fun", Fun);
        UIManager.Instance.EditBar("Energy", Energy);

        _petStartTime = DateTime.Now;

        StartCoroutine(PetTick());
    }

    //------------------------------------------------

    IEnumerator PetTick()
    {
        yield return new WaitForSeconds(decreaseTime);
        SetValues("HUNGER", false, decreaseAmount);
        SetValues("HYGIENE", false, decreaseAmount);
        SetValues("FUN", false, decreaseAmount);
        SetValues("ENERGY", false, decreaseAmount);
        CheckNeeds(Hunger);
        CheckNeeds(Hygiene);
        CheckNeeds(Fun);
        CheckNeeds(Energy);
        CheckLevel();

        StartCoroutine(PetTick());
    }

    //------------------------------------------------

    public void SetValues(string needToChange, bool add, float amount)
    {
        switch (needToChange)
        {
            case "HUNGER":
                if (add == true)
                {
                    _hunger = AddValue(_hunger, amount);
                    UIManager.Instance.EditBar("Hunger", Hunger);
                }
                else if (add == false)
                {
                    _hunger = SubtractValue(_hunger, amount);
                    UIManager.Instance.EditBar("Hunger", Hunger);
                }
                break;

            case "HYGIENE":
                if (add == true)
                {
                    _hygiene = AddValue(_hygiene, amount);
                    UIManager.Instance.EditBar("Hygiene", Hygiene);
                }
                else if (add == false)
                {
                    _hygiene = SubtractValue(_hygiene, amount);
                    UIManager.Instance.EditBar("Hygiene", Hygiene);
                }
                break;

            case "FUN":
                if (add == true)
                {
                    _fun = AddValue(_fun, amount);
                    UIManager.Instance.EditBar("Fun", Fun);
                }
                else if (add == false)
                {
                    _fun = SubtractValue(_fun, amount);
                    UIManager.Instance.EditBar("Fun", Fun);
                }

                CallOnFunNeedChange();

                break;

            case "ENERGY":
                if (add == true)
                {
                    _energy = AddValue(_energy, amount);
                    UIManager.Instance.EditBar("Energy", Energy);
                }
                else if (add == false)
                {
                    _energy = SubtractValue(_energy, amount);
                    UIManager.Instance.EditBar("Energy", Energy);
                }
                break;
        }
    }

    public float AddValue(float needValue, float amountToAdd)
    {
        float tempNeed = needValue;
        tempNeed = Mathf.Clamp(tempNeed + amountToAdd, 0f, 100f);

        return tempNeed;
    }

    public float SubtractValue(float needValue, float amountToSubtract)
    {
        float tempNeed = needValue;
        tempNeed = Mathf.Clamp(tempNeed - amountToSubtract, 0f, 100f);
        DeathCheck();

        return tempNeed;
    }

    private void DeathCheck()
    {
        lowNeeds = 0;

        if (Hunger == 0)
        {
            lowNeeds = lowNeeds + 1;
        }

        if (Hygiene == 0)
        {
            lowNeeds = lowNeeds + 1;
        }

        if (Fun == 0)
        {
            lowNeeds = lowNeeds + 1;
        }

        if (Energy == 0)
        {
            lowNeeds = lowNeeds + 1;
        }

        if (lowNeeds == 2)
        {
            PetManager.Instance.PetDeath();
        }
    }

    //------------------------------------------------

    private void CheckNeeds(float tempNeed)
    {
        if (tempNeed <= restingPoint && PetManager.Instance.CanCare == true)
        {
            restingPoint = restingPoint - 5;
            needsDictionary.Clear();
            SetDictionary();

            PetManager.Instance.LowNeed(needsDictionary);
        }
    }

    private void SetDictionary()
    {
        for (int i = 0; i <= 4; i++)
        {
            switch (i)
            {
                case 1:
                    needsDictionary.Add("HUNGER", Hunger);
                    break;

                case 2:
                    needsDictionary.Add("HYGIENE", Hygiene);
                    break;

                case 3:
                    needsDictionary.Add("FUN", Fun);
                    break;

                case 4:
                    needsDictionary.Add("ENERGY", Energy);
                    break;
            }
        }
    }

    //------------------------------------------------

    private void CheckLevel()
    {
        TimeSpan timeDif = DateTime.Now - _petStartTime;

        //if (Mathf.RoundToInt((float)timeDif.TotalHours) >= ((PetLevel + 1) * hoursInDay))
        //{
        //    _petLevel++;
        //    CallOnLevelUp();
        //}

        if (Mathf.RoundToInt((float)timeDif.TotalMinutes) >= ((PetLevel + 1) * minsInDay))
        {
            _petLevel++;
            CallOnLevelUp();
        }
    }

    public void SetPetLevelFromLoad()
    {
        TimeSpan timeDif = DateTime.Now - _petStartTime;

        _petLevel = timeDif.Minutes / minsInDay;

        // _petLevel = timeDif.Hours / hoursInDay; //TODO switch back to hours!
    }

    //------------------------------------------------
    //Event Functions

    protected void CallOnLevelUp()
    {
        if (OnPetLevelUp != null)
        {
            OnPetLevelUp();
        }
    }

    protected void CallOnFunNeedChange()
    {
        if (OnFunNeedChange != null)
        {
            OnFunNeedChange();
        }
    }
}
