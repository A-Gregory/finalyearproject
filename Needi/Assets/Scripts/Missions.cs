﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public abstract class MissionBase
{
    public string objective;
    public int reward;

    public void Init()
    {
        SetVariables();
        EventBinds();
    }

    protected abstract void SetVariables();

    protected abstract void EventBinds();

    public abstract void UnbindEvent();

    public abstract void IsMissionComplete();   
}

[Serializable]
public class Mission1 : MissionBase
{
    int requiredLevel;

    protected override void SetVariables()
    {
        requiredLevel = PetManager.Instance.petScript.PetLevel + 1;
        objective = "Your pet wants to be age " + requiredLevel + "!";
        reward = 12;
    }

    protected override void EventBinds()
    {
        Pet.OnPetLevelUp += IsMissionComplete;
    }

    public override void UnbindEvent()
    {
        Pet.OnPetLevelUp -= IsMissionComplete;      
    }

    public override void IsMissionComplete()
    {
        if(PetManager.Instance.petScript.PetLevel >= requiredLevel)
        {
            MissionManager.Instance.MissionComplete();
            UnbindEvent();
        }
    }
}

[Serializable]
public class Mission2 : MissionBase
{
    int requriedFunLevel = 90;

    protected override void SetVariables()
    {
        objective = "Your pet is feeling pretty bored, raise its fun level to " + requriedFunLevel + " !";
        reward = 25;
    }

    protected override void EventBinds()
    {
        Pet.OnFunNeedChange += IsMissionComplete;
    }

    public override void UnbindEvent()
    {
        Pet.OnFunNeedChange -= IsMissionComplete;
    }

    public override void IsMissionComplete()
    {
        if (PetManager.Instance.petScript.Fun >= requriedFunLevel)
        {
            MissionManager.Instance.MissionComplete();
            UnbindEvent();
        }
    }
}

[Serializable]
public class Mission3 : MissionBase
{
    protected override void SetVariables()
    {
        objective = "Your pet has some hunger cravings, buy him a Banana";
        reward = 10;
    }

    protected override void EventBinds()
    {
        ObjectManager.OnBuyObject += IsMissionComplete;
    }

    public override void UnbindEvent()
    {
        ObjectManager.OnBuyObject -= IsMissionComplete;
    }

    public override void IsMissionComplete()
    {
        if (ObjectManager.Instance.GameItems.ContainsKey("BANANA"))
        {
            MissionManager.Instance.MissionComplete();
            UnbindEvent();
        }
    }
}

[Serializable]
public class Mission4 : MissionBase
{
    int requiredScore = 6;

    protected override void SetVariables()
    {
        objective = "Play Pet Bounce and get " + requiredScore + " points!";
        reward = 20;
    }

    protected override void EventBinds()
    {
        GameManager.OnMinigameEnd += IsMissionComplete;
    }

    public override void UnbindEvent()
    {
        GameManager.OnMinigameEnd -= IsMissionComplete;
    }

    public override void IsMissionComplete()
    {
        if (GameInstance.Instance.petBounceScore >= requiredScore)
        {
            MissionManager.Instance.MissionComplete();
            UnbindEvent();
        }
    }
}

[Serializable]
public class Mission5 : MissionBase
{
    int requiredScore = 12;

    protected override void SetVariables()
    {
        objective = "Play Pet Run and get " + requiredScore + " points!";
        reward = 30;
    }

    protected override void EventBinds()
    {
        GameManager.OnMinigameEnd += IsMissionComplete;
    }

    public override void UnbindEvent()
    {
        GameManager.OnMinigameEnd -= IsMissionComplete;
    }

    public override void IsMissionComplete()
    {
        if (GameInstance.Instance.petRunScore >= requiredScore)
        {
            MissionManager.Instance.MissionComplete();
            UnbindEvent();
        }
    }
}
