﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node
{
    public List<Node> neighbourNodes;

    public Node parentNode;

    public Tile owner;

    public int G; // Movement cost 10 horizontal, 14 diagonal
    public int H; // Etimated distance to end point, count the squares multiply by 10
    public int F; // G + H

    //------------------------------------------------

    public Node()
    {
        neighbourNodes = new List<Node>();
    }

    //------------------------------------------------

    public void SetNeighbours()
    {
        neighbourNodes.Clear();

        neighbourNodes = owner.Neighbours();
    }

    public void SetScores(Tile currentTile, Tile endTile)
    {
        SetG();
        SetH(currentTile, endTile);
        SetF();
    }

    public void SetG()
    {
        if(Vector3.Distance(owner.gameObject.transform.position, parentNode.owner.gameObject.transform.position) <= 1)
        {
            G = parentNode.G + 10;
        }
        else
        {
            G = parentNode.G +  14;
        }
        
    }

    public void SetH(Tile currentTile, Tile endTile)
    {
        float xDistance = Mathf.Abs(currentTile.gameObject.transform.position.x - endTile.gameObject.transform.position.x);
        float zDistance = Mathf.Abs(currentTile.gameObject.transform.position.z - endTile.gameObject.transform.position.z);

        if (xDistance > zDistance)
        {
            H = (int)(14 * zDistance + 10 * (xDistance - zDistance));
        }
        else
        {
            H = (int)(14 * xDistance + 10 * (zDistance - xDistance));
        }
    }

    public void SetF()
    {
        F = G + H;
    }
}
