﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsBallScript : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            PetBounceGM.Instance.GameOver();
        }
    }
}
