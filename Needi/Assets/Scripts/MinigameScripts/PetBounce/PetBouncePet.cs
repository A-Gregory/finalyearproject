﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetBouncePet : MonoBehaviour
{
    public float speed = 25f;
    private Vector3 targetPos;
    private bool dragging;

    //------------------------------------------------

    public void Init()
    {
        targetPos = transform.position;
    }

    //------------------------------------------------

    void Update()
    {
        if (Input.GetMouseButtonDown(0) || Input.touches.Length > 0)
        {
            dragging = true;

        }

        if (dragging == true)
        {
            float newZ = transform.position.z - Camera.main.transform.position.z;
            targetPos = new Vector3(Input.mousePosition.x, 150, newZ);
            targetPos = Camera.main.ScreenToWorldPoint(targetPos);

            transform.position = Vector3.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);
        }


        if (Input.GetMouseButtonUp(0)) //|| Input.touches.Length == 0)//Comment out Input.touches to make work on PC
        {
            dragging = false;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        PetBounceGM.Instance.ScoreChange();
    }
}

