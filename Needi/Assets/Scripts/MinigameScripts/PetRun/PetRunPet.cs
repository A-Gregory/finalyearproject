﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetRunPet : MonoBehaviour
{
    public float movementSpeed = 5f;
    Rigidbody rb;

    bool canJump = true;
    bool doubleJump = false;

    //------------------------------------------------

    public void Init()
    {
        rb = gameObject.GetComponent<Rigidbody>();
        rb.constraints = RigidbodyConstraints.FreezeRotation;
    }

    //------------------------------------------------

    private void FixedUpdate()
    {
        if ((Input.GetMouseButtonDown(0) || Input.touches.Length > 0) && canJump == true && doubleJump == false)
        {
            canJump = false;
            doubleJump = true;
            rb.velocity = new Vector3(rb.velocity.x, 7, 0); //Mobile
        }
        else if ((Input.GetMouseButtonDown(0) || Input.touches.Length > 0) && canJump == false && doubleJump == true)
        {
            doubleJump = false;
            //rb.AddForce(new Vector3(0, 250, 0));
            rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y + 5, 0);
        }

        rb.velocity = new Vector3(movementSpeed, rb.velocity.y, 0);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            canJump = true;
            doubleJump = false;
        }
        else if (collision.gameObject.CompareTag("Hoop"))
        {
            PetRunGM.Instance.GameOver();
        }
        else if (collision.gameObject.CompareTag("ScoreZone"))
        {
            PetRunGM.Instance.ScoreChange();
        }
    }
}
