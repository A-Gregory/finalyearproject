﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    public GameObject pet;

    private void Update()
    {
        gameObject.transform.position = new Vector3((pet.gameObject.transform.position.x + 5    ), gameObject.transform.position.y, gameObject.transform.position.z);
    }
}
