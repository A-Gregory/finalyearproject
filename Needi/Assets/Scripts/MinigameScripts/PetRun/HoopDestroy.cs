﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoopDestroy : MonoBehaviour
{
    public GameObject destroyPoint = null;

    private void Update()
    {
        if (destroyPoint != null && transform.position.x < destroyPoint.transform.position.x)
        {
            Destroy(gameObject);
        }
    }
}
