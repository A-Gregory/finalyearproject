﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreZoneScript : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pedro"))
        {
            PetRunGM.Instance.ScoreChange();
        }
    }
}
