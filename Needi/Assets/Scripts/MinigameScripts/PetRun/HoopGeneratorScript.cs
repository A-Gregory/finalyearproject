﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoopGeneratorScript : MonoBehaviour
{
    public GameObject hoop;
    public Transform cameraPoint;
    public float minDistance = 10;
    public float maxDistance = 25;
    float distanceBetween;

    GameObject mostRecent;

    private void Update()
    {
        if (transform.position.x < cameraPoint.position.x)
        {
            distanceBetween = Random.Range(minDistance, maxDistance);
            transform.position = new Vector3((transform.position.x + distanceBetween), transform.position.y, transform.position.z);

            mostRecent = Instantiate(hoop, transform.position, transform.rotation) as GameObject;
            mostRecent.GetComponent<HoopDestroy>().destroyPoint = GameObject.Find("DeletePoint") as GameObject;
        }
    }
}
