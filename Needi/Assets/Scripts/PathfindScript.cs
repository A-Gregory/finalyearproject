﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathfindScript : MonoBehaviour
{
    public List<Tile> board = new List<Tile>();
    public List<Node> nodes = new List<Node>();

    public Tile startingTile;
    public Tile currentTile;
    public Tile endTile;
    public GameObject pathMarker;

    private List<Node> finalPath = new List<Node>();

    private bool pathFound;
    private bool findingPath = false;
    private bool moving = false;

    //------------------------------------------------

    public void Init()
    {
        foreach (GameObject temp in GameObject.FindGameObjectsWithTag("Tile"))
        {
            board.Add(temp.GetComponent<Tile>());
        }

        foreach (Tile temp in board)
        {
            nodes.Add(temp.Node);
        }

        RaycastHit hit;

        if (Physics.Raycast(transform.position, Vector3.down, out hit, 500f))
        {
            startingTile = hit.collider.gameObject.GetComponent<Tile>();
            print(startingTile.name);
        }

        currentTile = startingTile;
        TileInit();
    }

    //------------------------------------------------

    public void TileInit()
    {
       foreach (Tile temp in board)
        {
            temp.Init();
        }

        CheckNeighbours();
    }

    private void CheckNeighbours()
    {
        foreach (Tile temp in board)
        {
            temp.Node.SetNeighbours();
        }
    }

    //------------------------------------------------

    public void Pathfind(Tile chosenTile)
    {
        if (findingPath == false)
        {
            findingPath = true;

            List<Node> openList = new List<Node>();
            List<Node> closedList = new List<Node>();

            pathFound = false;
                openList.Add(currentTile.Node);

            endTile = chosenTile;

            currentTile.Node.parentNode = null;

            CheckNeighbours();

            foreach (Node temp in currentTile.Node.neighbourNodes)
            {
                openList.Add(temp);
                temp.parentNode = currentTile.Node;

                temp.SetScores(temp.owner, endTile);
            }

            openList.RemoveAt(openList.IndexOf(currentTile.Node));
            closedList.Add(currentTile.Node);

            while (pathFound == false)
            {
                int lowestFValue = 0;
                
                foreach (Node temp in openList)
                {
                    if (lowestFValue == 0)
                    {
                        lowestFValue = temp.F;
                        currentTile = temp.owner;
                    }
                    else if (temp.F < lowestFValue)
                    {
                        lowestFValue = temp.F;
                        currentTile = temp.owner;
                    }
                }

                openList.RemoveAt(openList.IndexOf(currentTile.Node));
                closedList.Add(currentTile.Node);

                foreach (Node temp in currentTile.Node.neighbourNodes)
                {
                    if (openList.Contains(temp))
                    {
                        if (temp.G + currentTile.Node.G < temp.G)
                        {
                            temp.parentNode = currentTile.Node;
                            temp.SetScores(temp.owner, endTile);
                        }
                    }
                    else if (closedList.Contains(temp))
                    {

                    }
                    else
                    {
                        openList.Add(temp);
                        temp.parentNode = currentTile.Node;
                        temp.SetScores(temp.owner, endTile);
                    }
                }

                foreach (Node temp in closedList)
                {
                    if (temp.owner == endTile)
                    {
                        currentTile = endTile;
                        pathFound = true;

                        while (currentTile.Node.parentNode != null)
                        {
                            finalPath.Add(currentTile.Node);
                            currentTile = currentTile.Node.parentNode.owner;
                        }
                    }
                }
            }

            currentTile = endTile;
            print(currentTile.gameObject.name);

            StartCoroutine(Movement());
        }

    }

    //------------------------------------------------

    IEnumerator Movement()
    {
        if (moving == false)
        {
            moving = true;
            AnimationManager.Instance.ToggleMove(); //Toggle Animation
            finalPath.Reverse();

            for (int i = 0; i < finalPath.Count; i++)
            {
                float t = 0f;
                Vector3 start = gameObject.transform.position;
                Vector3 end = new Vector3(finalPath[i].owner.transform.position.x, finalPath[i].owner.transform.position.y + 0.5f, finalPath[i].owner.transform.position.z);

                while (t <= 1.0f)
                {
                    t += (Time.deltaTime * 1.0f) * 1.5f;
                    gameObject.transform.position = Vector3.Lerp(start, end, t);
                    yield return new WaitForFixedUpdate();
                }
            }

            AnimationManager.Instance.ToggleMove(); //Toggle Animation
            finalPath.Clear();
            findingPath = false;
            moving = false;
            pathFound = false;

            PetManager.Instance.ActivateSelectedObject();
        }

    }
}
