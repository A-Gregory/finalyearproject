﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobileStateManager : SingletonBase<MobileStateManager>
{
    private void OnApplicationQuit()
    {
        FileManager.Instance.Save();
    }

    private void OnApplicationPause(bool pause)
    {
        if (pause == true)
        {
            FileManager.Instance.Save();
        }
    }

    private void OnApplicationFocus(bool focus)
    {
        if (focus == true)
        {
            //FileManager.Instance.Load(); //TODO add back before phone build!
        }
    }

}
