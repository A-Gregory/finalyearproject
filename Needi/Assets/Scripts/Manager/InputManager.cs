﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : SingletonBase<InputManager>
{
    private bool touch;
    private bool swipeLeft;
    private bool swipeRight;
    private bool dragging;

    private Vector2 startTouch;
    private Vector2 swipeDelta;

    //------------------------------------------------

    private void Update()
    {
        touch = false;
        swipeRight = false;
        swipeLeft = false;

        if (Input.GetMouseButtonDown(0))
        {
            touch = true;
            dragging = true;
            startTouch = Input.mousePosition;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            Reset();
        }

        if (Input.touches.Length > 0)
        {
            if (Input.touches[0].phase == TouchPhase.Began)
            {
                touch = true;
                dragging = true;
                startTouch = Input.touches[0].position;
            }
            else if (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled)
            {
                Reset();
            }
        }

        if (dragging == true)
        {
            if (Input.touches.Length > 0)
            {
                swipeDelta = Input.touches[0].position - startTouch;
            }
            else if (Input.GetMouseButton(0))
            {
                swipeDelta = (Vector2)Input.mousePosition - startTouch;
            }
        }

        if (swipeDelta.magnitude > 500)
        {
            float x = swipeDelta.x;
            float y = swipeDelta.y;

            if (Mathf.Abs(x) > Mathf.Abs(y))
            {
                if (x < 0)
                {
                    swipeLeft = true;
                    UIManager.Instance.Swiped("LEFT");
                }
                else if (x > 0)
                {
                    swipeRight = true;
                    UIManager.Instance.Swiped("RIGHT");
                }
            }
        }
    }

    //------------------------------------------------

    private void Reset()
    {
        startTouch = Vector2.zero;
        swipeDelta = Vector2.zero;
        dragging = false;
    }

}
