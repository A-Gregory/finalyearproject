﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PetRunGM : SingletonBase<PetRunGM>
{
    public static UIManager uiManager;
    public GameObject petSpawn;

    private  int score = 0;
    private HoopGeneratorScript hoopGeneratorScript;
    private PetRunPet petScript;
    private GameObject ui;
    private GameObject pet;
    private GameObject mainCamera;
    private GameObject hoopGenerator;

    //------------------------------------------------

    public void Awake()
    {
        Init();
    }

    //------------------------------------------------

    public override void Init()
    {
        base.Init();

        ui = Instantiate(Resources.Load("Prefab/PBUIManager")) as GameObject;

        pet = Instantiate(Resources.Load("Prefab/MiniGamePet")) as GameObject;
        pet.transform.position = petSpawn.transform.position;
        pet.AddComponent<Rigidbody>();
        petScript = pet.AddComponent<PetRunPet>();

        mainCamera = GameObject.Find("Main Camera").gameObject;
        mainCamera.GetComponent<CameraScript>().pet = pet;

        hoopGenerator = GameObject.Find("HoopGenerator");
        hoopGeneratorScript = hoopGenerator.GetComponent<HoopGeneratorScript>();

        SpawnManagers();
    }

    //------------------------------------------------

    private void SpawnManagers()
    {
        uiManager = ui.GetComponent<UIManager>();
        uiManager.startUI = "PB";

        ManagerInits();
    }

    private void ManagerInits()
    {
        uiManager.Init();
        petScript.Init();
    }

    public void ScoreChange()
    {
        score += 1;
        uiManager.SetInGameScore(score);

        if (score % 5 == 0)
        {
            petScript.movementSpeed += 2;
            hoopGeneratorScript.maxDistance -= 2;
        }
    }

    public void GameOver()
    {
        Destroy(pet);
        Destroy(mainCamera.GetComponent<CameraScript>());

        int tempMoney = GameInstance.Instance.ownerMoney;
        int newMoney = GameInstance.Instance.ownerMoney + (score * 5);
        GameInstance.Instance.petRunScore = score;
        GameInstance.Instance.moneyToAdd = score * 5;
        uiManager.ShowGameOverUI(tempMoney, score, newMoney);
    }
}
