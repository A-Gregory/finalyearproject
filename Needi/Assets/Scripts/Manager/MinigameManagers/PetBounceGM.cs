﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PetBounceGM : SingletonBase<PetBounceGM>
{
    public static UIManager uiManager;
    public GameObject ballSpawn;

    private int score = 0;
    private GameObject ui;
    private GameObject ball;
    private GameObject pet;

    //------------------------------------------------

    public void Awake()
    {
        Init();
    }

    //------------------------------------------------

    public override void Init()
    {
        base.Init();    

        ui = Instantiate(Resources.Load("Prefab/PBUIManager")) as GameObject;

        ball = Instantiate(Resources.Load("Prefab/PhysicsBall")) as GameObject;
        ball.transform.position = ballSpawn.transform.position;

        pet = Instantiate(Resources.Load("Prefab/MiniGamePet")) as GameObject;
        pet.AddComponent<PetBouncePet>();

        SpawnManagers();
    }

    //------------------------------------------------

    private void SpawnManagers()
    {
        uiManager = ui.GetComponent<UIManager>();
        uiManager.startUI = "PB";

        ManagerInits();
    }

    private void ManagerInits()
    {
        uiManager.Init();
        pet.GetComponent<PetBouncePet>().Init();
    }

    public void ScoreChange()
    {
        score += 1;
        uiManager.SetInGameScore(score);

        if (score % 10 == 0)
        {
            AddBall();
        }
    }

    public void GameOver()
    {
        Destroy(ball);
        Destroy(pet);

        int tempMoney = GameInstance.Instance.ownerMoney;
        int newMoney = GameInstance.Instance.ownerMoney + (score * 5);

        GameInstance.Instance.petBounceScore = score;
        GameInstance.Instance.moneyToAdd = score * 5;

        uiManager.ShowGameOverUI(tempMoney, score, newMoney);
    }

    private void AddBall()
    {
        ball = Instantiate(Resources.Load("Prefab/PhysicsBall")) as GameObject;
        ball.transform.position = ballSpawn.transform.position;
    }
}
