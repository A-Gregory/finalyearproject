﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class GameManager : SingletonBase<GameManager>
{
    //Events
    public delegate void MinigameDataEventArgs();
    public static event MinigameDataEventArgs OnMinigameEnd;

    public static PetManager petManager;
    public static ObjectManager objectManager;
    public static UIManager uiManager;
    public static InputManager inputManager;
    public static MapManager mapManager;
    public static FileManager fileManager;
    public static MobileStateManager msManager;
    public static Owner owner;
    public static AnimationManager animationManager;
    public static UnlockManager unlockManager;
    public static MissionManager missionManager;

    GameObject ui;

    public GameObject gameInstanceGO;

    //------------------------------------------------

    public void Awake()
    {
        Init();
    }

    //------------------------------------------------

    public override void Init()
    {
        base.Init();
        ui = Instantiate(Resources.Load("Prefab/UIManager")) as GameObject;
        SpawnManagers();
    }

    //------------------------------------------------

    private void SpawnManagers()
    {
        fileManager = gameObject.AddComponent<FileManager>();

        mapManager = gameObject.AddComponent<MapManager>();
        inputManager = gameObject.AddComponent<InputManager>();

        uiManager = ui.GetComponent<UIManager>();

        petManager = gameObject.AddComponent<PetManager>();
        objectManager = gameObject.AddComponent<ObjectManager>();
        unlockManager = gameObject.AddComponent<UnlockManager>();
        animationManager = gameObject.AddComponent<AnimationManager>();

        owner = gameObject.AddComponent<Owner>();
        missionManager = gameObject.AddComponent<MissionManager>();

        msManager = gameObject.AddComponent<MobileStateManager>();

        if (!(gameInstanceGO.GetComponent<GameInstance>()))
        {
            gameInstanceGO.AddComponent<GameInstance>();
            gameInstanceGO.GetComponent<GameInstance>().Init();
        }

        ManagerInits();
    }

    //------------------------------------------------

    private void ManagerInits()
    {
        fileManager.Init();

        msManager.Init();

        uiManager.Init();
        owner.Init();

        mapManager.Init();
        inputManager.Init();
            
        petManager.Init();
        objectManager.Init();
        unlockManager.Init();
        animationManager.Init();

        missionManager.Init();

        if (File.Exists(Application.persistentDataPath + "/savedData.dat"))
        {
            fileManager.Load();
        }
        else
        {
            missionManager.SelectNewMission();
        }

        if (GameObject.Find("GameInstance"))
        {
            Owner.Instance.EditMoney(true, GameInstance.Instance.moneyToAdd);

            if (GameInstance.Instance.petBounceScore > 0 || GameInstance.Instance.petRunScore > 0)
            {
                if (OnMinigameEnd != null)
                {
                    OnMinigameEnd();
                }
            }

            GameInstance.Instance.ResetValues();
            GameInstance.Instance.ownerMoney = Owner.Instance.Money;
        }          
    }
}
