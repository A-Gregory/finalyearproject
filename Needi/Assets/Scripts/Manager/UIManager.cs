﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : SingletonBase<UIManager>
{
    public string startUI;
    public GameObject ui;
    private GameObject shopUI;
    private GameObject minigameUI;
    public GameObject hungerBar;
    public GameObject funBar;
    public GameObject hygieneBar;
    public GameObject energyBar;
    public GameObject moneyText;
    public GameObject mainGameOverUI;
    public GameObject optionsMenu;
    public GameObject levelIndicator;
    public GameObject missionText;
    public GameObject missionReward;
    public GameObject missionArea;

    public GameObject minigameGameOverUI;
    public GameObject currentMoneyGO;
    public GameObject newMoneyGO;
    public GameObject scoreGO;
    public GameObject inGameScoreGO;

    public List<GameObject> shopButtons;
    public List<GameObject> shopPages;
    private int currentShopPage = 1;

    private Color green;
    private Color orange;
    private Color red;

    //------------------------------------------------

    public override void Init()
    {
        base.Init();

        SetColors();

        Owner.OnMoneyIncrease += UpdateMoney;
        Pet.OnPetLevelUp += EditLevel;
    }

    //------------------------------------------------
    //Showing UI Menus

    public void ShowItemUI()
    {
        ui.transform.Find("Rotate").gameObject.SetActive(true);
        ui.transform.Find("YesItem").gameObject.SetActive(true);
    }

    private void SwipeLeft()
    {
        ui.transform.Find("MinigameMenu").gameObject.SetActive(true);
    }

    private void SwipeRight()
    {
        ui.transform.Find("Shop").gameObject.SetActive(true);

        foreach (GameObject button in shopButtons)
        {
            ButtonScript buttonUnlockSript = button.GetComponent<ButtonScript>();

            if (buttonUnlockSript.unlocked == false)
            {
                buttonUnlockSript.UnlockCheck();
            }
        }
    }

    public void ShowGameOverUI(int currentMoney, int score, int newMoney)
    {
        Text currentMoneyText = currentMoneyGO.GetComponent<Text>();
        currentMoneyText.text = currentMoney.ToString();

        Text newMoneyText = newMoneyGO.GetComponent<Text>();
        newMoneyText.text = newMoney.ToString();

        Text scoreText = scoreGO.GetComponent<Text>();
        scoreText.text = score.ToString();

        minigameGameOverUI.SetActive(true);
    }

    public void ShowMainGameOverUI()
    {
        mainGameOverUI.SetActive(true);
    }

    public void ShowOptions()
    {
        if (optionsMenu.activeSelf == true)
        {
            optionsMenu.SetActive(false);
        }
        else if (optionsMenu.activeSelf == false)
        {
            optionsMenu.SetActive(true);
        }
    }

    //------------------------------------------------
    //Hiding UI Menus

    public void HideItemUI()
    {
        ui.transform.Find("Rotate").gameObject.SetActive(false);
        ui.transform.Find("YesItem").gameObject.SetActive(false);
    }

    public void ExitShop()
    {
        ui.transform.Find("Shop").gameObject.SetActive(false);
    }

    public void ExitMiniGameMenu()
    {
        ui.transform.Find("MinigameMenu").gameObject.SetActive(false);
    }

    public void HideOptions()
    {
        optionsMenu.SetActive(false);
    }

    public void ToggleMissionUI()
    {
        if (missionArea.active == false)
        {
            missionArea.SetActive(true);
        }
        else if (missionArea.active == true)
        {
            missionArea.SetActive(false);
        }
    }

    //------------------------------------------------
    //Button Presses

    public void BuyItem(string item)
    {
        ObjectManager.Instance.ItemPurchased(item);
    }

    public void StartPetBounce()
    {
        UnbindEvents();
        FileManager.Instance.Save();
        GameInstance.Instance.ChangeScene(1);
    }
        
    public void StartPetRun()
    {
        UnbindEvents();
        FileManager.Instance.Save();
        GameInstance.Instance.ChangeScene(2);
    }

    public void RotateItem()
    {
        ObjectManager.Instance.RotateItem();
    }

    public void ConfirmItem()
    {
        ObjectManager.Instance.ConfirmItemPosition();
    }

    public void LeaveGame()
    {
        UnbindEvents();
        GameInstance.Instance.ChangeScene(0);
    }

    public void RestartGame()
    {
        UnbindEvents();
        FileManager.Instance.DeleteSave();
        GameInstance.Instance.ChangeScene(0);
    }

    //------------------------------------------------
    //Set UI Values

    public void EditBar(string need, float Level)
    {
        float newLevel = Level / 100;
        Image needImage;

        switch (need)
        {
            case "Hunger":

                needImage = hungerBar.GetComponent<Image>();
                needImage.fillAmount = newLevel;

                SetIconBarColour(hungerBar.GetComponent<Image>(), newLevel);

                break;

            case "Hygiene":

                needImage = hygieneBar.GetComponent<Image>();
                needImage.fillAmount = newLevel;

                SetIconBarColour(hygieneBar.GetComponent<Image>(), newLevel);

                break;

            case "Fun":

                needImage = funBar.GetComponent<Image>();
                needImage.fillAmount = newLevel;

                SetIconBarColour(funBar.GetComponent<Image>(), newLevel);

                break;

            case "Energy":

                needImage = energyBar.GetComponent<Image>();
                needImage.fillAmount = newLevel;

                SetIconBarColour(energyBar.GetComponent<Image>(), newLevel);

                break;
        }
    }   

    public void UpdateMoney()
    {
        if (startUI == "MAIN")
        {
            moneyText.GetComponent<Text>().text = Owner.Instance.Money.ToString();
        }
    }

    public void EditLevel()
    {
        levelIndicator.GetComponent<Text>().text = PetManager.Instance.petScript.PetLevel.ToString();
    }

    public void SetMissionUI(string mission, int reward)
    {
        missionText.GetComponent<Text>().text = mission;

        missionReward.GetComponent<Text>().text = reward.ToString();
    }

    public void SetInGameScore(int score)
    {
        Text inGameScoreText = inGameScoreGO.GetComponent<Text>();
        inGameScoreText.text = score.ToString();
    }

    //------------------------------------------------

    public void Swiped(string direction)
    {
        switch (direction)
        {
            case "LEFT":
                SwipeLeft();
                break;

            case "RIGHT":
                SwipeRight();
                break;
        }
    }

    private void NextShopPage()
    {
        if (currentShopPage == 3)
        {
            shopPages[2].SetActive(true);
            shopPages[1].SetActive(false);
            shopPages[0].SetActive(false);
        }
        else if (currentShopPage == 2)
        {
            shopPages[2].SetActive(false);
            shopPages[1].SetActive(true);
            shopPages[0].SetActive(false);
        }
        else if (currentShopPage == 1)
        {
            currentShopPage = 1;
            shopPages[2].SetActive(false);
            shopPages[1].SetActive(false);
            shopPages[0].SetActive(true);
        }
    }

    public void CalculatePage(bool forward)
    {
        if (forward == true)
        {
            currentShopPage++;

            if (currentShopPage > shopPages.Count)
            {
                currentShopPage = 1;
            }
        }
        else if (forward == false)
        {
            currentShopPage--;

            if (currentShopPage < 1)
            {
                currentShopPage = shopPages.Count;
            }
        }

        NextShopPage();
    }

    protected void SetColors()
    {
        green = new Color(0.04f, 0.74f, 0.04f, 1.0f);
        orange = new Color(0.83f, 0.65f, 0.08f, 1.0f);
        red = new Color(0.84f, 0.16f, 0.16f, 1.0f);
    }

    protected void SetIconBarColour(Image iconBar, float value)
    {
        if (value >= 0.4)
        {
            iconBar.color = green;
        }
        else if (value < 0.4 && value >= 0.2)
        {
            iconBar.color = orange;
        }
        else if (value < 0.2)
        {
            iconBar.color = red;
        }
    }

    //------------------------------------------------

    protected void UnbindEvents()
    {
        Owner.OnMoneyIncrease -= UpdateMoney;
        Pet.OnPetLevelUp -= EditLevel;
    }
}
