﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class FileManager : SingletonBase<FileManager>
{
    //------------------------------------------------

    public void DeleteSave()
    {
        File.Delete(Application.persistentDataPath + "/savedData.dat");
    }

    //------------------------------------------------

    public void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/savedData.dat");
        print(Application.persistentDataPath);

        SavedData save = new SavedData();

        save.Money = Owner.Instance.Money;

        save.Hunger = PetManager.Instance.petScript.Hunger;
        save.Hygiene = PetManager.Instance.petScript.Hygiene;
        save.Energy = PetManager.Instance.petScript.Energy;
        save.Fun = PetManager.Instance.petScript.Fun;
        save.petLevel = PetManager.Instance.petScript.PetLevel;
        save.petStartTime = PetManager.Instance.petScript.PetStartTime;

        save.timeClosed = DateTime.Now;

        foreach (KeyValuePair<string, Vector3> GameItem in ObjectManager.Instance.GameItems)
        {
            SavedData.newVector temp;

            temp.x = GameItem.Value.x;
            temp.y = GameItem.Value.y;
            temp.z = GameItem.Value.z;

            save.gameItems.Add(GameItem.Key, temp);
        }

        foreach (KeyValuePair<string, Vector3> gameItem in ObjectManager.Instance.GameItems)
        {
            save.gameItemUses.Add(gameItem.Key, GameObject.Find(gameItem.Key).GetComponent<BaseObject>().uses);
        }

        if (MissionManager.Instance.currentMission != null)
        {
            save.setMission = MissionManager.Instance.currentMission;
        }
        else if (MissionManager.Instance.currentMission == null)
        {
            save.missionCompleteTime = MissionManager.Instance.timeMissionFinished;
        }

        bf.Serialize(file, save);
        file.Close();
    }

    //------------------------------------------------

    public void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/savedData.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/savedData.dat", FileMode.Open);
            SavedData save = (SavedData)bf.Deserialize(file);
            file.Close();

            Owner.Instance.Money = save.Money;
            Owner.Instance.EditMoneyUI();

            PetManager.Instance.petScript.Hunger = 0;
            PetManager.Instance.petScript.SetValues("HUNGER", true, save.Hunger);

            PetManager.Instance.petScript.Hygiene = 0;
            PetManager.Instance.petScript.SetValues("HYGIENE", true, save.Hygiene);

            PetManager.Instance.petScript.Energy = 0;
            PetManager.Instance.petScript.SetValues("ENERGY", true, save.Energy);

            PetManager.Instance.petScript.Fun = 0;
            PetManager.Instance.petScript.SetValues("FUN", true, save.Fun);

            //PetManager.Instance.petScript.PetLevel = save.petLevel; //TODO may have broke level stuff by commenting?
            PetManager.Instance.petScript.PetStartTime = save.petStartTime;
            PetManager.Instance.petScript.SetPetLevelFromLoad();
            UIManager.Instance.EditLevel();

            if (save.setMission != null)
            {
                MissionManager.Instance.currentMission = save.setMission;
                MissionManager.Instance.MissionActive();
            }
            else if (save.setMission == null)
            {
                MissionManager.Instance.timeMissionFinished = save.missionCompleteTime;
                StartCoroutine(MissionManager.Instance.NewMissionCheck());
            }

            DateTime opened = DateTime.Now;
            TimeSpan timeDiff = opened - save.timeClosed;

            for (int i = Mathf.RoundToInt((float)timeDiff.TotalMinutes) / 20; i > 0; i--)
            {
                PetManager.Instance.petScript.SetValues("HUNGER", false, 1);
                PetManager.Instance.petScript.SetValues("HYGIENE", false, 1);
                PetManager.Instance.petScript.SetValues("ENERGY", false, 1);
                PetManager.Instance.petScript.SetValues("FUN", false, 1);
            }

            ObjectManager.Instance.GameItems.Clear();   

            foreach (KeyValuePair<string, SavedData.newVector> GameItem in save.gameItems)
            {
                Vector3 temp;

                temp.x = GameItem.Value.x;
                temp.y = GameItem.Value.y;
                temp.z = GameItem.Value.z;

                ObjectManager.Instance.GameItems.Add(GameItem.Key, temp);
            }

            ObjectManager.Instance.LoadItems();

            foreach (KeyValuePair<string, int> gameItem in save.gameItemUses)
            {
                GameObject tempObject = GameObject.Find(gameItem.Key);

                tempObject.GetComponent<BaseObject>().uses = gameItem.Value;
            }
        }
    }
}

[Serializable]
class SavedData
{
    [Serializable]
    public struct newVector
    {
        public float x;

        public float y;

        public float z;
    }

    private bool _saved = false;
    public bool Saved
    {
        get { return _saved; }
        set { _saved = value; }
    }

    private int _playerMoney;
    public int Money
    {
        get { return _playerMoney; }
        set { _playerMoney = value; }
    }

    private float _hunger;
    public float Hunger
    {
        set { _hunger = value; }
        get { return _hunger; }
    }

    private float _hygiene;
    public float Hygiene
    {
        set { _hygiene = value; }
        get { return _hygiene; }
    }

    private float _fun;
    public float Fun
    {
        set { _fun = value; }
        get { return _fun; }
    }

    private float _energy;
    public float Energy
    {
        set { _energy = value; }
        get { return _energy; }
    }

    public DateTime timeClosed;
    public DateTime petStartTime;
    public int petLevel;
    public MissionBase setMission = null;
    public DateTime missionCompleteTime;

    public Dictionary<string, newVector> gameItems = new Dictionary<string, newVector>();
    public Dictionary<string, int> gameItemUses = new Dictionary<string, int>();

}
