﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : SingletonBase<MapManager>
{
    public List<Tile> board = new List<Tile>();

    //------------------------------------------------

    public override void Init()
    {
        base.Init();

        foreach (GameObject temp in GameObject.FindGameObjectsWithTag("Tile"))
        {
            board.Add(temp.GetComponent<Tile>());
        }
    }

    //------------------------------------------------

    public void Editing()
    {
        foreach(Tile temp in board)
        {
            temp.editing = true;
        }
    }

    public void Reset()
    {
        foreach (Tile temp in board)
        {
            temp.editing = false;
        }
    }
}
