﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PetManager : SingletonBase<PetManager>
{
    private bool _canCare = true;
    public bool CanCare { get { return _canCare; } }

    private bool _petDead = false;
    public bool PetDead{ set { _petDead = value; } get { return _petDead; } }

    private Dictionary<string, ObjectManager.Changes> advertisements = new Dictionary<string, ObjectManager.Changes>();

    private BaseObject selectedObject;
    public Pet petScript;
    private PathfindScript aStar;
    private NeedBasedAIScript needBasedScript;

    private bool canInteract = true;

    //------------------------------------------------

    public override void Init()
    {
        base.Init();
        GameObject hierachy = GameObject.Find("Board");
        GameObject pet = Instantiate(Resources.Load("Prefab/Pedro II"), hierachy.transform) as GameObject;
        pet.transform.position = new Vector3(-1, 0.5f, -1);

        petScript = pet.GetComponent<Pet>();
        petScript.Init();

        aStar = pet.GetComponent<PathfindScript>();
        aStar.Init();

        needBasedScript = pet.GetComponent<NeedBasedAIScript>();    
    }

    //------------------------------------------------

    public void EditStats(ObjectManager.Changes changesStruct)
    {
        petScript.SetValues(changesStruct.NeedToChange, changesStruct.Addition, changesStruct.Amount);
    }

    public void LowNeed(Dictionary<string, float> needs)
    {
        if(CanCare == true)
        {
            _canCare = false;

            string itemToUse;
            GameObject itemToUseGO;
            BaseObject itemToUseObjectScript;

            advertisements = ObjectManager.Instance.GetAdvertisedNeeds();

            itemToUse = needBasedScript.CalculateActions(advertisements, needs);

            if (itemToUse != "ERROR")
            {
                itemToUseGO = GameObject.Find(itemToUse);
                itemToUseObjectScript = itemToUseGO.GetComponent<BaseObject>();

               PathfindAction(itemToUseObjectScript.actionTile, itemToUseObjectScript);
            }

            StartCoroutine(ResetAI());
        }
    }

    public void PathfindAction(Tile actionTile, BaseObject selectedObject)
    {
        aStar.Pathfind(actionTile);
        this.selectedObject = selectedObject;
    }

    public void ActivateSelectedObject()
    {
        if (selectedObject != null)
        {
            selectedObject.ActivatingObject();
        }
    }

    //------------------------------------------------

    IEnumerator ResetAI()
    {
        yield return new WaitForSeconds(30);

        _canCare = true;
    }

    //------------------------------------------------

    public void PetDeath()
    {
        UIManager.Instance.ShowMainGameOverUI();
    }
}
