﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameInstance : SingletonBase<GameInstance>
{ 
    public int petBounceScore;
    public int petRunScore;
    public int moneyToAdd;
    public int ownerMoney;

    //------------------------------------------------

    public override void Init()
    {
        base.Init();

        DontDestroyOnLoad(gameObject);

        ResetValues();
    }

    //------------------------------------------------

    public void ResetValues()
    {
        petBounceScore = 0;
        petRunScore = 0;
        moneyToAdd = 0;
        ownerMoney = 0;
    }

    //------------------------------------------------

    public void ChangeScene(int sceneNumber)
    {
        SceneManager.LoadScene(sceneNumber);
        MissionManager.Instance.UnbindAll();
    }
}
