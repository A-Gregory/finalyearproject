﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationManager : SingletonBase<AnimationManager>
{
    private Animator playerAnimator;

    //------------------------------------------------

    public override void Init()
    {
        base.Init();

        playerAnimator = PetManager.Instance.petScript.gameObject.GetComponent<Animator>();
    }

    //------------------------------------------------

    public void ToggleMove()
    {
        if (playerAnimator.GetBool("Movement") == false)
        {
            playerAnimator.SetBool("Movement", true);
        }
        else if (playerAnimator.GetBool("Movement") == true)
        {
            playerAnimator.SetBool("Movement", false);
        }
    }
}
