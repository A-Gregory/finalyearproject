﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class ObjectManager : SingletonBase<ObjectManager>
{
    public struct Changes
    {
        private string _needToChange;
        public string NeedToChange { set { _needToChange = value; } get { return _needToChange; } }

        private bool _addition;
        public bool Addition { set { _addition = value; } get { return _addition; } }

        private float _amount;
        public float Amount { set { _amount = value; } get { return _amount; } }
    }

    //------------------------------------------------

    //Events
    public delegate void ObjectManagerEventArgs();
    public static event ObjectManagerEventArgs OnBuyObject;

    public Dictionary<string, Vector3> GameItems = new Dictionary<string, Vector3>();

    private List<GameObject> ObjectsInWorld = new List<GameObject>();
    private GameObject objectToPlace;
    private Costs costs = new Costs();

    //------------------------------------------------

    private void Update()
    {
        print(GameItems.Count);
    }

    public override void Init()
    {
        base.Init();

        costs.SetCosts();

        GameObject[] tempArray = GameObject.FindGameObjectsWithTag("Object");

        if (!File.Exists(Application.persistentDataPath + "/savedData.dat"))
        {
            //GameItems.Add("T1BED", new Vector3(0, 0.8f, -2));
        }

        LoadItems();
    }

    //------------------------------------------------

    public void ObjectPressed(Changes changesStruct)
    {
        PetManager.Instance.EditStats(changesStruct);
    }

    public void PetInteract(string objectToInteract)
    {
        for (int i = 0; i < ObjectsInWorld.Count; i++)
        {
            BaseObject tempObject = ObjectsInWorld[i].GetComponent<BaseObject>();

            if (tempObject.name == objectToInteract)
            {
                tempObject.ActivatingObject();
            }
        }
    }

    public Dictionary<string, Changes> GetAdvertisedNeeds()
    {
        Dictionary<string, Changes> tempDictionary = new Dictionary<string, Changes>();
        Changes tempStruct;
        string tempName;

        foreach (GameObject tempObject in ObjectsInWorld)
        {
            BaseObject tempObjectScript = tempObject.GetComponent<BaseObject>();

            tempStruct = tempObjectScript.AdvertiseValues();
            tempName = tempObjectScript.objectName;

            tempDictionary.Add(tempName, tempStruct);
        }

        return tempDictionary;
    }

    //------------------------------------------------

    public void ItemPurchased(string item)
    {
        if (Owner.Instance.Money >= costs.GetCost(item) && PetManager.Instance.petScript.PetLevel >= UnlockManager.Instance.GetUnlockLevel(item))
        {
            objectToPlace = Instantiate(Resources.Load("Prefab/" + item)) as GameObject;
            objectToPlace.name = objectToPlace.GetComponent<BaseObject>().objectName;

            if (GameItems.ContainsKey(objectToPlace.name))
            {
                Destroy(objectToPlace);
                objectToPlace = null;
            }
            else
            {
                Owner.Instance.EditMoney(false, costs.GetCost(item));
                UIManager.Instance.UpdateMoney();
                UIManager.Instance.ExitShop();
                MapManager.Instance.Editing();
            }
        }
        else
        {
            Destroy(objectToPlace);
            objectToPlace = null;
        }     
    }

    public void MoveItem(GameObject tile)
    {
        objectToPlace.gameObject.transform.position = new Vector3(tile.transform.position.x, 0.85f, tile.transform.position.z);
    }

    public void PlaceItem(GameObject tile)
    {
        objectToPlace.transform.position = new Vector3(tile.transform.position.x, 0.85f, tile.transform.position.z);
        SetTiles(tile, objectToPlace);

        MapManager.Instance.Reset();
        UIManager.Instance.ShowItemUI();
    }

    public void ConfirmItemPosition()
    {
        GameItems.Add(objectToPlace.name, objectToPlace.transform.position);
        ObjectsInWorld.Add(objectToPlace);
        UIManager.Instance.HideItemUI();
        objectToPlace = null;

        CallOnBuyObject();
    }

    public void RotateItem()
    {
        int temp = Mathf.RoundToInt(objectToPlace.transform.eulerAngles.y);
        BaseObject tempObjectScript = objectToPlace.GetComponent<BaseObject>();

        if (tempObjectScript.rotation == "FORWARD")
        {
            tempObjectScript.itemTile.GetComponent<Tile>().ChangeActionTile("RIGHT");
            tempObjectScript.Rotated();

            tempObjectScript.rotation = "RIGHT";
        }
        else if (tempObjectScript.rotation == "RIGHT")
        {
            tempObjectScript.itemTile.GetComponent<Tile>().ChangeActionTile("LEFT");
            tempObjectScript.Rotated();

            tempObjectScript.rotation = "LEFT";
        }
        else if (tempObjectScript.rotation == "LEFT")
        {
            tempObjectScript.itemTile.GetComponent<Tile>().ChangeActionTile("FORWARD");
            tempObjectScript.Rotated();

            tempObjectScript.rotation = "FORWARD";
        }
    }

    private void SetTiles(GameObject tile, GameObject item)
    {
        tile.GetComponent<Tile>().SetActionTileColour();
        item.GetComponent<BaseObject>().Init();
    }

    //------------------------------------------------

    public void DeleteItems()
    {
        foreach(GameObject temp in ObjectsInWorld)
        {
            Destroy(temp);
        }
    }

    public void RemoveItem(BaseObject itemToRemove)
    {
        ObjectsInWorld.Remove(itemToRemove.gameObject);

        foreach (KeyValuePair<string, Vector3> gameItem in GameItems)
        {
            if (gameItem.Key == itemToRemove.objectName)
            {
                GameItems.Remove(itemToRemove.objectName);
                break;
            }
        }
    }

    //------------------------------------------------

    public void LoadItems()
    {
        foreach (KeyValuePair<string, Vector3> GameItem in GameItems)
        {
            GameObject temp = Instantiate(Resources.Load("Prefab/" + GameItem.Key)) as GameObject;
            BaseObject tempScript = temp.GetComponent<BaseObject>();

            temp.name = tempScript.objectName;
            temp.transform.position = GameItem.Value;

            ObjectsInWorld.Add(temp);

            tempScript.Init();
            SetTiles(tempScript.itemTile.gameObject, temp);
        }
    }

    //------------------------------------------------
    //Event Functions

    protected void CallOnBuyObject()
    {
        if (OnBuyObject != null)
        {
            OnBuyObject();
        }
    }

}

//------------------------------------------------

public class Costs
{
    private Dictionary<string, int> ItemCosts = new Dictionary<string, int>();

    public void SetCosts()
    {
        ItemCosts.Add("T1TOY", 15);
        ItemCosts.Add("T1BED", 15);
        ItemCosts.Add("T1BATH", 15);
        ItemCosts.Add("T1FOOD", 15);

        ItemCosts.Add("T2TOY", 25);
        ItemCosts.Add("T2BED", 25);
        ItemCosts.Add("T2BATH", 25);
        ItemCosts.Add("T2FOOD", 25);

        ItemCosts.Add("BUBBLEBLOWER", 15);
        ItemCosts.Add("HOSEPIPE", 5);
        ItemCosts.Add("BANANA", 5);
        ItemCosts.Add("ENERGYDRINK", 15);
    }

    public int GetCost(string item)
    {
        int temp = 0;

        foreach (KeyValuePair<string,int> itemCosts in ItemCosts)
        {
            if (itemCosts.Key == item)
            {
                temp = itemCosts.Value;
            }
        }

        return temp;
    }
}
