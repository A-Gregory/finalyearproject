﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MissionManager : SingletonBase<MissionManager>
{
    public List<MissionBase> MissionList = new List<MissionBase>();

    public MissionBase currentMission = null;
    public DateTime timeMissionFinished;

    //------------------------------------------------

    public override void Init()
    {
        base.Init();

        MissionList.Add(new Mission1());
        MissionList.Add(new Mission2());
        MissionList.Add(new Mission3());
        MissionList.Add(new Mission4());
        MissionList.Add(new Mission5());
    }

    //------------------------------------------------

    private void SetMissionUI()
    {
        UIManager.Instance.SetMissionUI(currentMission.objective, currentMission.reward);
    }

    public void SelectNewMission()
    {
        int selectedMission = UnityEngine.Random.Range(0, MissionList.Count);
        currentMission = MissionList[selectedMission];

        MissionActive();
    }

    public void MissionActive()
    {
        currentMission.Init();
        SetMissionUI();
        UIManager.Instance.ToggleMissionUI();
    }

    public IEnumerator NewMissionCheck()
    {
        yield return new WaitForSeconds(1);

        TimeSpan timeDifference = DateTime.Now - timeMissionFinished;
        if (timeDifference.Hours >= 3)        //timeDifference.Minutes >= 1) 
        {
            StopCoroutine(NewMissionCheck());
            SelectNewMission();
        }
        else
        {
            StartCoroutine(NewMissionCheck());
        }
    }

    //------------------------------------------------

    public void MissionComplete()
    {
        UIManager.Instance.ToggleMissionUI();
        Owner.Instance.EditMoney(true, currentMission.reward);
        timeMissionFinished = DateTime.Now;
        currentMission = null;
        StartCoroutine(NewMissionCheck());
    }

    public void UnbindAll()
    {
        foreach(MissionBase mission in MissionList)
        {
            mission.UnbindEvent();
        }
    }
}
