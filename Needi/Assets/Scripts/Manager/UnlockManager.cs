﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockManager : SingletonBase<UnlockManager>
{
    private Unlocks unlocks = new Unlocks();

    //------------------------------------------------

    public override void Init()
    {
        base.Init();

        unlocks.SetUnlocks();
    }

    //------------------------------------------------

    public int GetUnlockLevel(string item)
    {
        return unlocks.GetUnlockLevel(item);
    }
}

//------------------------------------------------

public class Unlocks
{
    private Dictionary<string, int> itemUnlocks = new Dictionary<string, int>();

    public void SetUnlocks()
    {
        itemUnlocks.Add("ENERGYDRINK", 1);
        itemUnlocks.Add("BUBBLEBLOWER", 1);
        itemUnlocks.Add("T2FOOD", 1);
        itemUnlocks.Add("T2BATH", 1);

        itemUnlocks.Add("T2BED", 2);
        itemUnlocks.Add("T2TOY", 2);
    }

    public int GetUnlockLevel(string item)
    {
        int temp = 0;

        foreach(KeyValuePair<string, int> itemUnlock in itemUnlocks)
        {
            if (itemUnlock.Key == item)
            {
                temp = itemUnlock.Value;
            }
        }

        return temp;
    }
}
