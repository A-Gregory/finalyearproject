﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Tile : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    public enum TileState {Walkable, NotWalkable, ActionTile, Hovered}

    private Node _node;
    public Node Node { get { return _node; } }

    //Initialised in inspector
    public TileState currentState;
    public Material noWalk;
    public Material actionSquare;
    public Material itemPlace;
    public Material defaultMat;

    public Tile actionTile = null;
    public bool editing;

    private GameObject player;
    private PathfindScript pathfindScript;
    private MeshRenderer tileMeshRenderer;

    //------------------------------------------------

    public void Init()
    {
        _node = new Node();

        _node.owner = this;
        player = PetManager.Instance.petScript.gameObject;
        pathfindScript = player.GetComponent<PathfindScript>();
        tileMeshRenderer = gameObject.GetComponent<MeshRenderer>();

        FindAndSetActionTile();
        StateChange();
    }

    //------------------------------------------------

    public void StateChange()
    {
        if (currentState == TileState.NotWalkable)
        {
            tileMeshRenderer.material = noWalk;
        }
        else if (currentState == TileState.ActionTile)
        {
            tileMeshRenderer.material = actionSquare;
        }
        else if (currentState == TileState.Walkable)
        {
            tileMeshRenderer.material = defaultMat;
        }
        else if (currentState == TileState.Hovered)
        {
            tileMeshRenderer.material = itemPlace;
        }
    }

    public List<Node> Neighbours()
    {
        List<Node> tempList = new List<Node>();

        Collider[] tempArray = Physics.OverlapBox(gameObject.transform.position, new Vector3(1, 1, 1), Quaternion.identity);

        foreach (Collider temp in tempArray)
        {
            if (temp.gameObject.CompareTag("Tile") && temp.GetComponent<Tile>() != this && temp.gameObject.GetComponent<Tile>().currentState == TileState.Walkable || temp.gameObject.CompareTag("Tile") && temp.GetComponent<Tile>() != this && temp.gameObject.GetComponent<Tile>().currentState == TileState.ActionTile)
             {  
                tempList.Add(temp.GetComponent<Tile>().Node);
             }
        }
        return tempList;
    }

    public void ChangeActionTile(string faceDirection)
    {
        RaycastHit hit;

        switch (faceDirection)
        {
            case "RIGHT":

                actionTile.currentState = TileState.Walkable;
                actionTile.StateChange();

                if (Physics.Raycast(gameObject.transform.position, Vector3.right, out hit, 1))
                {
                    actionTile = hit.collider.gameObject.GetComponent<Tile>();
                }

                SetActionTileColour();

                break;

            case "LEFT":

                actionTile.currentState = TileState.Walkable;
                actionTile.StateChange();

                if (Physics.Raycast(gameObject.transform.position, Vector3.left, out hit, 1))
                {
                    actionTile = hit.collider.gameObject.GetComponent<Tile>();
                }

                SetActionTileColour();

                break;

            case "FORWARD":

                actionTile.currentState = TileState.Walkable;
                actionTile.StateChange();

                if (Physics.Raycast(gameObject.transform.position, Vector3.back, out hit, 1))
                {
                    actionTile = hit.collider.gameObject.GetComponent<Tile>();
                }

                SetActionTileColour();

                break;
        }
    }

    public void SetActionTileColour()
    {
        actionTile.currentState = Tile.TileState.ActionTile;
        actionTile.StateChange();
    }

    public void ResetTileMat()
    {
        currentState = TileState.Walkable;
        StateChange();
    }

    private void FindAndSetActionTile()
    {
        RaycastHit hit;

        if (Physics.Raycast(gameObject.transform.position, Vector3.back, out hit, 1))
        {
            actionTile = hit.collider.gameObject.GetComponent<Tile>();
        }
    }

    //------------------------------------------------
    //Pointer Functions

    public void OnPointerClick(PointerEventData eventData)
    {
        if (editing == false)
        {
            if (currentState == TileState.Walkable || currentState == TileState.ActionTile)
            {
                pathfindScript.Pathfind(this);
            }
        }
        else if (editing == true)
        {
            if (currentState == TileState.Hovered && actionTile != null && actionTile.currentState == TileState.Hovered)
            {
                currentState = TileState.NotWalkable;
                StateChange();
                ObjectManager.Instance.PlaceItem(gameObject);

                actionTile.currentState = Tile.TileState.ActionTile;
                actionTile.StateChange();
            }
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (editing == true && actionTile != null)
        {
            if (currentState == TileState.Walkable && actionTile.currentState == TileState.Walkable)
            {
                currentState = TileState.Hovered;
                StateChange();

                actionTile.currentState = TileState.Hovered;
                actionTile.StateChange();

                ObjectManager.Instance.MoveItem(gameObject);
            }
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (editing == true && actionTile != null)
        {
            if (currentState == TileState.Hovered && actionTile.currentState == TileState.Hovered)
            {
                currentState = TileState.Walkable;
                StateChange();

                actionTile.currentState = TileState.Walkable;
                actionTile.StateChange();
            }
        }
    }
}
