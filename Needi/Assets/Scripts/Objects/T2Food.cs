﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class T2Food : BaseObject
{
    //------------------------------------------------

    public override void Init()
    {
        base.Init();

        maxUses = 7;

        needToChange = "HUNGER";
        addition = true;
        amount = 30f;

        secondNeedToChange = "HYGIENE";
        secondAddition = false;
        secondAmount = 15f;

        advertisedNeedToChange = needToChange;
        advertisedAddition = addition;
        advertisedAmount = amount;
    }

    //------------------------------------------------

    public override void ActivatingObject()
    {
        ActivateObject(needToChange, addition, amount);

        ActivateObject(secondNeedToChange, secondAddition, secondAmount);

        uses++;
        CheckItemUses();
    }

    protected override void ActivateObject(string needToChange, bool addition, float amount)
    {
        changesStruct.NeedToChange = needToChange;
        changesStruct.Addition = addition;
        changesStruct.Amount = amount;

        ObjectManager.Instance.ObjectPressed(changesStruct);
    }

    public override ObjectManager.Changes AdvertiseValues()
    {
        ObjectManager.Changes Advertised = new ObjectManager.Changes();

        Advertised.NeedToChange = advertisedNeedToChange;
        Advertised.Addition = advertisedAddition;
        Advertised.Amount = advertisedAmount;

        return Advertised;
    }
}
