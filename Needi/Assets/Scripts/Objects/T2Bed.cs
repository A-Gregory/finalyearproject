﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class T2Bed : BaseObject
{
    //------------------------------------------------

    public override void Init()
    {
        base.Init();

        maxUses = 7;

        needToChange = "ENERGY";
        addition = true;
        amount = 30f;

        secondNeedToChange = "HUNGER";
        secondAddition = false;
        secondAmount = 15f;

        advertisedNeedToChange = needToChange;
        advertisedAddition = addition;
        advertisedAmount = amount;
    }

    //------------------------------------------------

    public override void ActivatingObject()
    {
        ActivateObject(needToChange, addition, amount);

        ActivateObject(secondNeedToChange, secondAddition, secondAmount);

        uses++;
        CheckItemUses();
    }

    protected override void ActivateObject(string needToChange, bool addition, float amount)
    {
        changesStruct.NeedToChange = needToChange;
        changesStruct.Addition = addition;
        changesStruct.Amount = amount;

        ObjectManager.Instance.ObjectPressed(changesStruct);
    }

    public override ObjectManager.Changes AdvertiseValues()
    {
        ObjectManager.Changes Advertised = new ObjectManager.Changes();

        Advertised.NeedToChange = advertisedNeedToChange;
        Advertised.Addition = advertisedAddition;
        Advertised.Amount = advertisedAmount;

        return Advertised;
    }
}
