﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public abstract class BaseObject : MonoBehaviour, IPointerClickHandler   
{
    public ObjectManager.Changes changesStruct;

    public int unlockLevel = 0;
    public int uses = 0;
    public string objectName = "CHANGE ME";

    public Tile itemTile;
    public Tile actionTile;

    public string rotation = "FORWARD";

    protected int maxUses = 0;
    protected string needToChange;
    protected bool addition;
    protected float amount;

    protected string secondNeedToChange;
    protected bool secondAddition;
    protected float secondAmount;

    protected string advertisedNeedToChange;
    protected bool advertisedAddition;
    protected float advertisedAmount;

    //------------------------------------------------

    public virtual void Init()
    {
        FindAndSetObjectTiles();

        gameObject.layer = 10;
    }

    //------------------------------------------------

    private void FindAndSetObjectTiles()
    {
        RaycastHit hit;

        if (Physics.Raycast(gameObject.transform.position, Vector3.down, out hit, 100))
        {
            itemTile = hit.collider.gameObject.GetComponent<Tile>();
            itemTile.currentState = Tile.TileState.NotWalkable;
            itemTile.StateChange();
            actionTile = itemTile.actionTile;
        }
    }

    //------------------------------------------------

    public virtual void OnPointerClick(PointerEventData eventData)
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 100))
        {
            if (hit.collider.gameObject.tag == "Object")
            {
                MoveTo();
            }
        }
    }

    public void MoveTo()
    {
        PetManager.Instance.PathfindAction(actionTile, this);
    }

    public void CheckItemUses()
    {
        if (uses == maxUses)
        {
            ObjectManager.Instance.RemoveItem(this);
            itemTile.ResetTileMat();
            actionTile.ResetTileMat();
            Destroy(gameObject);
        }
    }

    //------------------------------------------------

    public void Rotated()
    {
        actionTile = itemTile.actionTile;
    }

    //------------------------------------------------
    //Abstract functions

    public abstract void ActivatingObject();

    protected abstract void ActivateObject(string needToChange, bool addition, float amount);

    public abstract ObjectManager.Changes AdvertiseValues();
}
