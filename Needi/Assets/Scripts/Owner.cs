﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Owner : SingletonBase<Owner>
{
    //Events
    public delegate void MoneyIncreasedArgs();
    public static event MoneyIncreasedArgs OnMoneyIncrease;

    private int _money = 25;
    public int Money{ set { _money = value; } get { return _money; } }

    //------------------------------------------------

    public void EditMoney(bool add, int amount)
    {
        if (add == true)
        {
            Money = Money + amount;
        }
        else if (add == false)
        {
            Money = Money - amount;
        }

        EditMoneyUI();
    }

    public void EditMoneyUI()
    {
        if (OnMoneyIncrease != null)
        {
            OnMoneyIncrease();
        }
    }
}
