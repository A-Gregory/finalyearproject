﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeedBasedAIScript : MonoBehaviour
{
    public string CalculateActions(Dictionary<string, ObjectManager.Changes> advertisements, Dictionary<string, float> needs)
    {
        string chosenAction = "ERROR";
        float highestScore = 0;
        float score;
        float futureValue;
        float increaseFraction;
        float currentFraction;

        foreach(KeyValuePair<string, ObjectManager.Changes> advertisementPair in advertisements)
        {
            score = 0;
            futureValue = 0;
            increaseFraction = 0;
            currentFraction = 0;

            foreach(KeyValuePair<string, float> needPairs in needs)
            {
                if (needPairs.Key == advertisementPair.Value.NeedToChange)
                {
                    if (advertisementPair.Value.Addition == true)
                    {
                        futureValue = Mathf.Clamp(needPairs.Value + advertisementPair.Value.Amount, 0f,100f);
                        increaseFraction = 10 / futureValue;
                        currentFraction = 10 / needPairs.Value;
                        score = currentFraction - increaseFraction;
                    }
                    else if (advertisementPair.Value.Addition == false)
                    {
                        futureValue = Mathf.Clamp(needPairs.Value - advertisementPair.Value.Amount, 0f, 100f);
                        increaseFraction = 10 / futureValue;
                        currentFraction = 10 / needPairs.Value;
                        score = currentFraction - increaseFraction;
                    }
                }
            }

            if (score > highestScore)
            {
                highestScore = score;
                chosenAction = advertisementPair.Key;
            }
        }

        return chosenAction;
    }
}
