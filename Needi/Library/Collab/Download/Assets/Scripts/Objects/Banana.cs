﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Banana : BaseObject
{
    public override void Init()
    {
        base.Init();

        cost = 5;

        needToChange = "HUNGER";
        addition = true;
        amount = 20f;

        advertisedNeedToChange = needToChange;
        advertisedAddition = addition;
        advertisedAmount = amount;
    }

    public override void ActivatingObject()
    {
        base.ActivatingObject();

        ActivateObject(needToChange, addition, amount);
    }

    public override void ActivateObject(string needToChange, bool addition, float amount)
    {
        changesStruct.NeedToChange = needToChange;
        changesStruct.Addition = addition;
        changesStruct.Amount = amount;

        ObjectManager.Instance.ObjectPressed(changesStruct);
    }

    public override ObjectManager.Changes AdvertiseValues()
    {
        ObjectManager.Changes Advertised = new ObjectManager.Changes();

        Advertised.NeedToChange = advertisedNeedToChange;
        Advertised.Addition = advertisedAddition;
        Advertised.Amount = advertisedAmount;

        return Advertised;
    }
}
