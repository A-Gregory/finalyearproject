﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pet : MonoBehaviour
{
    private float _hunger = 70;
    public float Hunger
    {
        set { _hunger = value; }
        get { return _hunger; }
    }

    private float _hygiene = 70;
    public float Hygiene
    {
        set { _hygiene = value; }
        get { return _hygiene; }
    }

    private float _fun = 70;
    public float Fun
    {
        set { _fun = value; }
        get { return _fun; }
    }

    private float _energy = 70;
    public float Energy
    {
        set { _energy = value; }
        get { return _energy; }
    }

    private float _restingPoint = 40;
    public float RestingPoint
    {
        set { _restingPoint = value; }
        get { return _restingPoint; }
    }

    Dictionary<string, float> needsDictionary = new Dictionary<string, float>();

    //UI stuff, needs to get changed at some point!
    public Text HungerScore;
    public Text HygieneScore;
    public Text FunScore;
    public Text EnergyScore;

    //Probably don't need to be public, look into this for alpha
    public float decreaseTime;
    public float decreaseAmount;
    private int lowNeeds = 0;

    public void Init()
    {
        UIManager.Instance.editBar("Hunger", Hunger);
        UIManager.Instance.editBar("Hygiene", Hygiene);
        UIManager.Instance.editBar("Fun", Fun);
        UIManager.Instance.editBar("Energy", Energy);

        StartCoroutine(Tick());
    }

    IEnumerator Tick()
    {
        yield return new WaitForSeconds(decreaseTime);
        SetValues("HUNGER", false, decreaseAmount);
        SetValues("HYGIENE", false, decreaseAmount);
        SetValues("FUN", false, decreaseAmount);
        SetValues("ENERGY", false, decreaseAmount);
        CheckNeeds(Hunger);
        CheckNeeds(Hygiene);
        CheckNeeds(Fun);
        CheckNeeds(Energy);
        StartCoroutine(Tick());
    }

    public void  SetValues(string needToChange, bool add, float amount)
    {
        switch (needToChange)
        {
            case "HUNGER":
                if (add == true)
                {
                    Hunger = addValue(Hunger, amount);
                    UIManager.Instance.editBar("Hunger", Hunger);
                }
                else if (add == false)
                {
                    Hunger = subtractValue(Hunger, amount);
                    UIManager.Instance.editBar("Hunger", Hunger);
                }
                break;

            case "HYGIENE":
                if (add == true)
                {
                    Hygiene = addValue(Hygiene, amount);
                    UIManager.Instance.editBar("Hygiene", Hygiene);
                }
                else if (add == false)
                {
                    Hygiene = subtractValue(Hygiene, amount);
                    UIManager.Instance.editBar("Hygiene", Hygiene);
                }
                break;

            case "FUN":
                if (add == true)
                {
                    Fun = addValue(Fun, amount);
                    UIManager.Instance.editBar("Fun", Fun);
                }
                else if (add == false)
                {
                    Fun = subtractValue(Fun, amount);
                    UIManager.Instance.editBar("Fun", Fun);
                }
                break;

            case "ENERGY":
                if (add == true)
                {
                    Energy = addValue(Energy, amount);
                    UIManager.Instance.editBar("Energy", Energy);
                }
                else if (add == false)
                {
                    Energy = subtractValue(Energy, amount);
                    UIManager.Instance.editBar("Energy", Energy);
                }
                break;
        }
    }

    public float addValue(float needValue, float amount)
    {
        float tempNeed = needValue;
        tempNeed = Mathf.Clamp(tempNeed + amount, 0f, 100f);

        return tempNeed;
    }

    public float subtractValue(float needValue, float amount)
    {
        float tempNeed = needValue;
        tempNeed = Mathf.Clamp(tempNeed - amount, 0f, 100f);
        DeathCheck();

        return tempNeed;
    }


    private void CheckNeeds(float tempNeed)
    {
        if (tempNeed <= RestingPoint && PetManager.Instance.canCare == true)
        {
            RestingPoint = RestingPoint - 5;
            needsDictionary.Clear();
            SetDictionary();

            PetManager.Instance.LowNeed(needsDictionary);
        }
    }

    private void DeathCheck()
    {
        lowNeeds = 0;

        if (Hunger == 0)
        {
            lowNeeds = lowNeeds + 1;
        }

        if (Hygiene == 0)
        {
            lowNeeds = lowNeeds + 1;
        }

        if (Fun == 0)
        {
            lowNeeds = lowNeeds + 1;
        }

        if (Energy == 0)
        {
            lowNeeds = lowNeeds + 1;
        }

        if (lowNeeds == 2)
        {
            print("GAME OVER");
            PetManager.Instance.PetDead = true;
        }
    }

    private void SetDictionary()
    {
        for(int i = 0; i<=4; i++)
        {
            switch(i)
            {
                case 1:
                    needsDictionary.Add("HUNGER", Hunger);
                    break;

                case 2:
                    needsDictionary.Add("HYGIENE", Hygiene);
                    break;

                case 3:
                    needsDictionary.Add("FUN", Fun);
                    break;

                case 4:
                    needsDictionary.Add("ENERGY", Energy);
                    break;
            }
        }
    }
}
