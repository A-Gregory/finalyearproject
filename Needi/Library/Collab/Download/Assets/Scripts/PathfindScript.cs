﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathfindScript : MonoBehaviour
{
    public List<Tile> board = new List<Tile>();
    public List<Node> nodes = new List<Node>();
    List<Node> finalPath = new List<Node>();

    public Tile startingTile;
    public Tile currentTile;
    public Tile endTile;

    public GameObject pathMarker;

    bool pathFound;
    bool findingPath = false;
    bool moving = false;

    public void Init()
    {
        foreach (GameObject temp in GameObject.FindGameObjectsWithTag("Tile"))
        {
            board.Add(temp.GetComponent<Tile>());
        }

        foreach (Tile temp in board)
        {
            nodes.Add(temp.node);
        }

        RaycastHit hit;

        if (Physics.Raycast(transform.position, Vector3.down, out hit, 500f))
        {
            startingTile = hit.collider.gameObject.GetComponent<Tile>();
            print(startingTile.name);
        }

        currentTile = startingTile;
        TileInit();
    }

    public void TileInit()
    {
       foreach (Tile temp in board)
        {
            temp.Init();
        }

        CheckNeighbours();
    }

    private void CheckNeighbours()
    {
        foreach (Tile temp in board)
        {
            temp.node.SetNeighbours();
        }
    }

    public void Pathfind(Tile chosenTile)
    {
        if (findingPath == false)
        {
            findingPath = true;

            List<Node> openList = new List<Node>();
            List<Node> closedList = new List<Node>();

            pathFound = false;
                openList.Add(currentTile.node);

            endTile = chosenTile;

            currentTile.node.parentNode = null;

            CheckNeighbours();

            foreach (Node temp in currentTile.node.neighbourNodes)
            {
                openList.Add(temp);
                temp.parentNode = currentTile.node;

                temp.SetScores(temp.owner, endTile);
            }

            openList.RemoveAt(openList.IndexOf(currentTile.node));
            closedList.Add(currentTile.node);

            while (pathFound == false)
            {
                int lowestFValue = 0;
                
                foreach (Node temp in openList)
                {
                    if (lowestFValue == 0)
                    {
                        lowestFValue = temp.F;
                        currentTile = temp.owner;
                    }
                    else if (temp.F < lowestFValue)
                    {
                        lowestFValue = temp.F;
                        currentTile = temp.owner;
                    }
                }

                openList.RemoveAt(openList.IndexOf(currentTile.node));
                closedList.Add(currentTile.node);

                foreach (Node temp in currentTile.node.neighbourNodes)
                {
                    if (openList.Contains(temp))
                    {
                        if (temp.G + currentTile.node.G < temp.G)
                        {
                            temp.parentNode = currentTile.node;
                            temp.SetScores(temp.owner, endTile);
                        }
                    }
                    else if (closedList.Contains(temp))
                    {

                    }
                    else
                    {
                        openList.Add(temp);
                        temp.parentNode = currentTile.node;
                        temp.SetScores(temp.owner, endTile);
                    }
                }

                foreach (Node temp in closedList)
                {
                    if (temp.owner == endTile)
                    {
                        currentTile = endTile;
                        pathFound = true;

                        while (currentTile.node.parentNode != null)
                        {
                            finalPath.Add(currentTile.node);
                            currentTile = currentTile.node.parentNode.owner;
                        }
                    }
                }
            }

            currentTile = endTile;
            print(currentTile.gameObject.name);

            StartCoroutine(Movement());
        }

    }
    
    IEnumerator Movement()
    {
        if (moving == false)
        {
            moving = true;
            AnimationManager.Instance.ToggleMove(); //Toggle Animation
            finalPath.Reverse();

            for (int i = 0; i < finalPath.Count; i++)
            {
                float t = 0f;
                Vector3 start = gameObject.transform.position;
                Vector3 end = new Vector3(finalPath[i].owner.transform.position.x, finalPath[i].owner.transform.position.y + 0.5f, finalPath[i].owner.transform.position.z);

                while (t <= 1.0f)
                {
                    t += (Time.deltaTime * 1.0f) * 1.5f;
                    gameObject.transform.position = Vector3.Lerp(start, end, t);
                    yield return new WaitForFixedUpdate();
                }
            }

            AnimationManager.Instance.ToggleMove(); //Toggle Animation
            finalPath.Clear();
            findingPath = false;
            moving = false;
            pathFound = false;
        }

    }
}
