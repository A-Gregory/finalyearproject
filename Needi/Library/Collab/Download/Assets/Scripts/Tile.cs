﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Tile : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    public enum TileState {Walkable, NotWalkable, ActionTile}
    public TileState currentState;

    public Tile actionTile = null;

    public bool editing;

    public Text GScore;
    public Text HScore;
    public Text FScore;

    public Node node;

    public GameObject player;

    public Material noWalk;
    public Material actionSquare;
    public Material itemPlace;
    public Material defaultMat;

    public void Init ()
    {
        node = new Node();
        node.owner = this;

        player = GameObject.FindGameObjectWithTag("Pedro") as GameObject;

        RaycastHit hit;

        if (Physics.Raycast(gameObject.transform.position, Vector3.back, out hit, 1))
        {
            actionTile = hit.collider.gameObject.GetComponent<Tile>();
        }

        StateChange();
    }
	
	void Update ()
    {
        GScore.text = node.G.ToString();
        HScore.text = node.H.ToString();
        FScore.text = node.F.ToString();
	}

    public void StateChange()
    {
        if (currentState == TileState.NotWalkable)
        {
            gameObject.GetComponent<MeshRenderer>().material = noWalk;
        }
        else if (currentState == TileState.ActionTile)
        {
            gameObject.GetComponent<MeshRenderer>().material = actionSquare;
        }
        else if (currentState == TileState.Walkable)
        {
            gameObject.GetComponent<MeshRenderer>().material = defaultMat;
        }
    }

    public List<Node> Neighbours()
    {
        List<Node> tempList = new List<Node>();

        Collider[] tempArray = Physics.OverlapBox(gameObject.transform.position, new Vector3(1, 1, 1), Quaternion.identity);

        foreach (Collider temp in tempArray)
        {

            if (temp.gameObject.CompareTag("Tile") && temp.GetComponent<Tile>() != this && temp.gameObject.GetComponent<Tile>().currentState == TileState.Walkable || temp.gameObject.CompareTag("Tile") && temp.GetComponent<Tile>() != this && temp.gameObject.GetComponent<Tile>().currentState == TileState.ActionTile)
             {  
                tempList.Add(temp.GetComponent<Tile>().node);
             }
        }
        return tempList;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
            if (editing == false)
            {
                if (currentState == TileState.Walkable || currentState == TileState.ActionTile)
                {
                    player.GetComponent<PathfindScript>().Pathfind(this);
                }
            }
            else if (editing == true)
            {
                if (currentState == TileState.Walkable && actionTile != null && actionTile.currentState == TileState.Walkable)
                {
                    currentState = TileState.NotWalkable;
                    StateChange();
                    ObjectManager.Instance.placeItem(gameObject);

                    actionTile.currentState = Tile.TileState.ActionTile;
                    actionTile.StateChange();
                }
            }       
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (editing == true && actionTile != null)
        {
                if (currentState == TileState.Walkable && actionTile.currentState == TileState.Walkable)
                {
                    gameObject.GetComponent<MeshRenderer>().material = itemPlace;
                    actionTile.GetComponent<MeshRenderer>().material = itemPlace;
                    ObjectManager.Instance.moveItem(gameObject);
                }
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (editing == true && actionTile != null)
        {
            if (currentState == TileState.Walkable && actionTile.currentState == TileState.Walkable)
            {
                gameObject.GetComponent<MeshRenderer>().material = defaultMat;
                actionTile.GetComponent<MeshRenderer>().material = defaultMat;
            }
        }
    }

    public void ChangeActionTile(string faceD)
    {
        RaycastHit hit;

        switch (faceD)
        {

            case "RIGHT":

                actionTile.currentState = TileState.Walkable;
                actionTile.StateChange();

                if (Physics.Raycast(gameObject.transform.position, Vector3.right, out hit, 1))
                {
                    actionTile = hit.collider.gameObject.GetComponent<Tile>();
                }

                SetActionTileColour();

                break;

            case "LEFT":

                actionTile.currentState = TileState.Walkable;
                actionTile.StateChange();

                if (Physics.Raycast(gameObject.transform.position, Vector3.left, out hit, 1))
                {
                    actionTile = hit.collider.gameObject.GetComponent<Tile>();
                }

                SetActionTileColour();

                break;

            case "FORWARD":

                actionTile.currentState = TileState.Walkable;
                actionTile.StateChange();

                if (Physics.Raycast(gameObject.transform.position, Vector3.back, out hit, 1))
                {
                    actionTile = hit.collider.gameObject.GetComponent<Tile>();
                }

                SetActionTileColour();

                break;
        }
    }

    public void SetActionTileColour()
    {
        actionTile.currentState = Tile.TileState.ActionTile;
        actionTile.StateChange();
    }
}
