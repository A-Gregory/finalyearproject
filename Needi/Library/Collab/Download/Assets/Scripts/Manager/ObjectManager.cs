﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class ObjectManager : SingletonBase<ObjectManager>
{
    public struct Changes
    {
        private string _needToChange;
        public string NeedToChange
        {
            set { _needToChange = value; }
            get { return _needToChange; }
        }

        private bool _addition;
        public bool Addition
        {
            set { _addition = value; }
            get { return _addition; }
        }

        private float _amount;
        public float Amount
        {
            set { _amount = value; }
            get { return _amount; }
        }
    }

    private List<GameObject> ObjectsInWorld = new List<GameObject>();
    private GameObject objectToPlace;

    public Dictionary<string, Vector3> GameItems = new Dictionary<string, Vector3>();

    private List<string> ObjectPrefabNames = new List<string>();

    Costs costs = new Costs();

    public override void Init()
    {
        base.Init();

        costs.setCosts();

        GameObject[] tempArray = GameObject.FindGameObjectsWithTag("Object");

        if (!File.Exists(Application.persistentDataPath + "/savedData.dat"))
        {
            GameItems.Add("BED", new Vector3(0, 0.8f, -2));
            //GameItems.Add("OVEN", new Vector3(1, 0.8f, -2));
            //GameItems.Add("BATH", new Vector3(2, 0.8f, -2));
            //GameItems.Add("BALL", new Vector3(3, 0.8f, -2));
        }

        LoadItems();
    }

    public void ObjectPressed(Changes changesStruct)
    {
        PetManager.Instance.EditStats(changesStruct);
    }

    public Dictionary<string, Changes> GetAdvertisedNeeds()
    {
        Dictionary<string, Changes> tempDictionary = new Dictionary<string, Changes>();
        Changes tempStruct;
        string tempName;

        foreach (GameObject temp in ObjectsInWorld)
        {
            BaseObject tempBO = temp.GetComponent<BaseObject>();

            tempStruct = tempBO.AdvertiseValues();
            tempName = tempBO.name;

            tempDictionary.Add(tempName, tempStruct);
        }

        return tempDictionary;
    }

    public void PetInteract(string objectToInteract)
    {
        foreach (GameObject item in ObjectsInWorld)
        {
            if (item.GetComponent<BaseObject>().name == objectToInteract)
            {
                item.GetComponent<BaseObject>().ActivatingObject();
            }
        }
    }

    public void itempurchased(string item)
    {
        if (Owner.Instance.Money >= costs.getCost(item))
        {
            objectToPlace = Instantiate(Resources.Load("Prefab/" + item)) as GameObject;
            objectToPlace.name = objectToPlace.GetComponent<BaseObject>().name;
            UIManager.Instance.exitShop();
            MapManager.Instance.editing();
        }
        else
        {
            print("Not Enough Money To Buy " + item);
            Destroy(objectToPlace);
            objectToPlace = null;
        }
       
    }

    public void moveItem(GameObject tile)
    {
        objectToPlace.gameObject.transform.position = new Vector3(tile.transform.position.x, 0.85f, tile.transform.position.z);
    }

    public void placeItem(GameObject tile)
    {
        objectToPlace.transform.position = new Vector3(tile.transform.position.x, 0.85f, tile.transform.position.z);
        ObjectPrefabNames.Add(objectToPlace.name);
        GameItems.Add(objectToPlace.name, objectToPlace.transform.position);
        ObjectsInWorld.Add(objectToPlace);
        setTiles(tile, objectToPlace);

        MapManager.Instance.Reset();
        UIManager.Instance.showItemUI();
    }

    public void confirmItem()
    {
        UIManager.Instance.hideItemUI();
        objectToPlace = null;
    }

    public void rotateItem()
    {
        int temp = Mathf.RoundToInt(objectToPlace.transform.eulerAngles.y);
        BaseObject tempScript = objectToPlace.GetComponent<BaseObject>();

        if (temp == 0)
        {
            objectToPlace.transform.eulerAngles = new Vector3(objectToPlace.transform.eulerAngles.x, -90.0f, objectToPlace.transform.eulerAngles.z);
            tempScript.itemTile.GetComponent<Tile>().ChangeActionTile("RIGHT");
            tempScript.Rotated();
        }
        else if (temp == 270)
        {
            objectToPlace.transform.eulerAngles = new Vector3(objectToPlace.transform.eulerAngles.x, 90.0f, objectToPlace.transform.eulerAngles.z);
            tempScript.itemTile.GetComponent<Tile>().ChangeActionTile("LEFT");
            tempScript.Rotated();
        }
        else if (temp == 90)
        {
            objectToPlace.transform.eulerAngles = new Vector3(objectToPlace.transform.eulerAngles.x, 0.0f, objectToPlace.transform.eulerAngles.z);
            tempScript.itemTile.GetComponent<Tile>().ChangeActionTile("FORWARD");
            tempScript.Rotated();
        }
    }

    private void setTiles(GameObject tile, GameObject item)
    {
        tile.GetComponent<Tile>().SetActionTileColour();
        item.GetComponent<BaseObject>().Init();
    }

    public void LoadItems()
    {
        foreach(KeyValuePair<string, Vector3> GameItem in GameItems)
        {
            //print("Trying to Spawn " + GameItem.Key);
            GameObject temp = Instantiate(Resources.Load("Prefab/" + GameItem.Key)) as GameObject;
            BaseObject tempScript = temp.GetComponent<BaseObject>();
            temp.name = tempScript.name;
            temp.transform.position = GameItem.Value;
            ObjectsInWorld.Add(temp);
            tempScript.Init();
            setTiles(tempScript.itemTile.gameObject, temp);
        }
    }

    public void deleteItems()
    {
        foreach(GameObject temp in ObjectsInWorld)
        {
            Destroy(temp);
        }
    }
}

public class Costs
{
    private Dictionary<string, int> ItemCosts = new Dictionary<string, int>();

    public void setCosts()
    {
        ItemCosts.Add("BALL", 5);
        ItemCosts.Add("BANANA", 5);
        ItemCosts.Add("BATH", 5);
        ItemCosts.Add("BED", 5);
        ItemCosts.Add("OVEN", 5);
    }

    public int getCost(string item)
    {
        int temp = 0;

        foreach (KeyValuePair<string,int> itemCosts in ItemCosts)
        {
            if (itemCosts.Key == item)
            {
                temp = itemCosts.Value;
            }
        }

        return temp;
    }
}
