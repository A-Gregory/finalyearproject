﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class GameManager : SingletonBase<GameManager>
{
    public static PetManager petManager;
    public static ObjectManager objectManager;
    public static UIManager uiManager;
    public static InputManager inputManager;
    public static MapManager mapManager;
    public static FileManager fileManager;
    public static MobileStateManager msManager;
    public static Owner owner;
    public static AnimationManager animationManager;

    public GameObject ownerGO;

    public bool isSave;

    GameObject ui;

    public void Awake()
    {
        Init();
    }

    public override void Init()
    {
        base.Init();
        ui = Instantiate(Resources.Load("Prefab/UIManager")) as GameObject;
        SpawnManagers();
    }
    
    private void SpawnManagers()
    {
        fileManager = gameObject.AddComponent<FileManager>();

        mapManager = gameObject.AddComponent<MapManager>();
        inputManager = gameObject.AddComponent<InputManager>();

        uiManager = ui.GetComponent<UIManager>();
        uiManager.startUI = "MAIN";

        petManager = gameObject.AddComponent<PetManager>();
        objectManager = gameObject.AddComponent<ObjectManager>();
        animationManager = gameObject.AddComponent<AnimationManager>();

        owner = ownerGO.AddComponent<Owner>();

        msManager = gameObject.AddComponent<MobileStateManager>();

        ManagerInits();
    }

    private void ManagerInits()
    {
        fileManager.Init();

        msManager.Init();

        mapManager.Init();
        uiManager.Init();
        inputManager.Init();
            
        petManager.Init();
        objectManager.Init();
        animationManager.Init();

        owner.Init();

        if (File.Exists(Application.persistentDataPath + "/savedData.dat"))
        {
            fileManager.Load();
        }
    }


}
